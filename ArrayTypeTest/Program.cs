﻿using System;
using System.Collections.Generic;
using System.Reflection;

public static class DeepCopy
{
	[Flags]
	public enum CopyOptions
	{
		PublicSerialised		= 0x0,
		CopyPrivateFields		= 0x1,
		CopyNonSerialisedFields	= 0x2,
		CopyAll					= ~0x0
	}

	// non array versions
	public static void SourceToTarget< TypeFrom, TypeTo >( TypeFrom source, TypeTo target )
	where TypeTo : new() where TypeFrom : TypeTo 
	=> Copy( source, target, CopyOptions.CopyAll, new Type[ 0 ], new Dictionary< System.Object, System.Object >() );

	public static void SourceToTargetWithFilter< TypeFrom, TypeTo >( TypeFrom source, TypeTo target, Type[] arrayOfMemberTypesToNotCopy )
	where TypeTo : new() where TypeFrom : TypeTo
	=> Copy( source, target, CopyOptions.CopyAll, arrayOfMemberTypesToNotCopy, new Dictionary< System.Object, System.Object >() );

	public static void SourceToTargetWithOptions< TypeFrom, TypeTo >( TypeFrom source, TypeTo target, CopyOptions options )
	where TypeTo : new() where TypeFrom : TypeTo
	=> Copy( source, target, options, new Type[0], new Dictionary< System.Object, System.Object >() );

	public static void SourceToTargetWithOptionsFiltered< TypeFrom, TypeTo >( TypeFrom source, TypeTo target, CopyOptions options, Type[] arrayOfMemberTypesToNotCopy )
	where TypeTo : new() where TypeFrom : TypeTo
	=> Copy( source, target, options, arrayOfMemberTypesToNotCopy, new Dictionary< System.Object, System.Object >() );

	public static TypeToClone Clone< TypeToClone >( TypeToClone instanceToClone ) where TypeToClone : new()
	{
		if( TryCreateDefaultConstructedObject( typeof(TypeToClone), out var createdOrNullIfError ) )
		{
			Copy( instanceToClone, createdOrNullIfError, CopyOptions.CopyAll, new Type[0], new Dictionary< System.Object, System.Object >() );
		}

		return(TypeToClone) createdOrNullIfError;
	}

	// array versions
	public static void SourceToTarget< TypeFrom, TypeTo >( TypeFrom[] source, TypeTo[] target )
	where TypeTo : new() where TypeFrom : TypeTo
	=> Copy( source, target, CopyOptions.CopyAll, new Type[0], new Dictionary< System.Object, System.Object >() );

	public static void SourceToTargetWithFilter< TypeFrom, TypeTo >( TypeFrom[] source, TypeTo[] target, Type[] arrayOfMemberTypesToNotCopy )
	where TypeTo : new() where TypeFrom : TypeTo
	=> Copy( source, target, CopyOptions.CopyAll, arrayOfMemberTypesToNotCopy, new Dictionary< System.Object, System.Object >() );

	public static void SourceToTargetWithOptions< TypeFrom, TypeTo >( TypeFrom[] source, TypeTo[] target, CopyOptions options )
	where TypeTo : new() where TypeFrom : TypeTo
	=> Copy( source, target, options, new Type[0], new Dictionary< System.Object, System.Object >() );

	public static void SourceToTargetWithOptionsFiltered< TypeFrom, TypeTo >( TypeFrom[] source, TypeTo[] target, CopyOptions options, Type[] arrayOfMemberTypesToNotCopy )
	where TypeTo : new() where TypeFrom : TypeTo
	=> Copy( source, target, options, arrayOfMemberTypesToNotCopy, new Dictionary< System.Object, System.Object >() );

	public static TypeToClone[] Clone< TypeToClone >( TypeToClone[] instanceToClone ) where TypeToClone : new()
	{
		var clonedArray = new TypeToClone[ instanceToClone.Length ];
		Copy( instanceToClone, clonedArray, CopyOptions.CopyAll, new Type[0], new Dictionary< System.Object, System.Object >() );

		return clonedArray;
	}

	private static bool TryCreateDefaultConstructedObject( Type typeToCreate, out Object outCreatedObjectOrNull )
	{
		try
		{
			outCreatedObjectOrNull = Activator.CreateInstance( typeToCreate );
		}
		catch( Exception e )
		{
			Debug.Log( $"exception when default constructing {typeToCreate} - {e.Message}" );
			outCreatedObjectOrNull = null;
			return false;
		}

		return true;
	}

	private static void Copy( System.Object source, System.Object target, CopyOptions copyOptions, Type[] arrayOfTypesToNotCopy, Dictionary< System.Object, System.Object> dictCachedObjectCopies, int recursionCounter = 0 )
	{	
		#region local functions																															    

		// is this a value or a string? (i.e. built in language types which can be treated as values assignable with '=')
		bool IsPrimitive( Type type ) => type.IsValueType || ( type == typeof(string) );

		// check typeToCheck is not in arrayOfTypesToNotCopy
		bool TypeIsNotExcludedFromCopying( Type typeToCheck )
		{
			return ( Array.FindIndex( arrayOfTypesToNotCopy, tid => tid.IsAssignableFrom( typeToCheck ) ) < 0 ); // find index returns -1 if not found
		}

		// checks for a specific flag from CopyOptions
		bool CopyOptionIsTrue( CopyOptions toCheck )
		{
			return 0 != ( copyOptions & toCheck );
		}

		// checks if the copy is on an array
		bool ThisIsAnArrayCopyOperation() => source.GetType().IsArray;

		// gets previously cached copy of the passe object if it exists
		bool TryGetPreviouslyCachedCopyOfObject( System.Object objectToCopy, out System.Object cachedCopyOfObjectOrNull )
		{
			if( dictCachedObjectCopies.TryGetValue( objectToCopy, out var cachedCopy ) )
			{
				cachedCopyOfObjectOrNull = cachedCopy;
				return true;
			}

			cachedCopyOfObjectOrNull = null;
			return false;
		}

		// if above function returns false, use this to create a default constructed object and add to the cache
		bool TryCreateDefaultConstructedObjectOfSameTypeAndAddToCache( System.Object sourceObject, out System.Object outCreatedObjectOrNull )
		{
			if( TryCreateDefaultConstructedObject( sourceObject.GetType(), out outCreatedObjectOrNull ) )
			{
				dictCachedObjectCopies.Add( sourceObject, outCreatedObjectOrNull );
			}
			return ( null != outCreatedObjectOrNull );
		}


		// handles copy of an Array
		void CopyArray( Array sourceArray, Array targetArray, Type arrayElementType, string logFieldPrefixArrayName )
		{
			for( int i = 0; i < sourceArray.Length; ++i )
			{
				var sourceArrayElement = sourceArray.GetValue( i );

				if( IsPrimitive( arrayElementType ) )
				{
					targetArray.SetValue( sourceArrayElement, i );
				}
				else
				{
					if( null == sourceArrayElement )
					{
						Debug.Log( $"{logFieldPrefixArrayName}[{i}] == null - not copying" );
						continue;
					}

					if( TryGetPreviouslyCachedCopyOfObject( sourceArrayElement, out var alreadyCachedCopy ) )
					{
						targetArray.SetValue( alreadyCachedCopy, i );
					}
					else
					{
						if( TryCreateDefaultConstructedObjectOfSameTypeAndAddToCache( sourceArrayElement, out var defaultConstructed ) )
						{
							targetArray.SetValue( defaultConstructed, i );
							Copy( sourceArray.GetValue( i ), defaultConstructed, copyOptions, arrayOfTypesToNotCopy, dictCachedObjectCopies, recursionCounter );
						}
					}
				}
			}
		}

		#endregion local functions

		// keep track of recursion for indenting logging
		const string k_indentStringPostfix = ">>";
		var logIndent = k_indentStringPostfix.PadLeft( recursionCounter + k_indentStringPostfix.Length );
		recursionCounter++;

		// both objects being arrays is a special case
		if( ThisIsAnArrayCopyOperation() )
		{
			CopyArray( source as Array, target as Array, source.GetType().GetElementType(), $"{logIndent} #### type: {source.GetType().Name} - array copy" );			
		}
		else
		{
			var shouldCopyPrivateFields			= CopyOptionIsTrue( CopyOptions.CopyPrivateFields );
			var shouldCopyNonSerialisedFields	= CopyOptionIsTrue( CopyOptions.CopyNonSerialisedFields );

			var fieldInfoToCopyArray = source.GetType().GetFields( BindingFlags.Instance | BindingFlags.Public | ( shouldCopyPrivateFields ? BindingFlags.NonPublic : 0 ) );
			Debug.Log( $"{logIndent}#### type: {source.GetType().Name} NumFields: {fieldInfoToCopyArray.Length}" );

			int fieldIndex = 0;
			foreach( var fieldInfo in fieldInfoToCopyArray )
			{
				var fieldTypeToCopy = fieldInfo.FieldType;

				var logFieldPrefix = $"{logIndent}[{fieldIndex++}] {fieldTypeToCopy}:{fieldInfo.Name} ";

				var fieldIsNotSerialised = ( 0 != ( fieldInfo.Attributes & FieldAttributes.NotSerialized ) );

				if( fieldIsNotSerialised &&  ( ! shouldCopyNonSerialisedFields ) )
				{
					Debug.Log( $"{logFieldPrefix} - not serialized && copyOnlySerialisedFields == true : ignoring" );
				}
				else
				{
					// arrays need special handling	- can't just overwrite an array
					if( fieldTypeToCopy.IsArray )
					{
						Debug.Log( $"{logFieldPrefix} - is an array" );

						var arrayElementType	= fieldTypeToCopy.GetElementType();
						var sourceArray			= fieldInfo.GetValue( source ) as Array;

						if( null != sourceArray )
						{
							if( ! TypeIsNotExcludedFromCopying( arrayElementType ) )
							{
								Debug.Log( $"{logFieldPrefix} - array element type is in NO COPY list ignoring" );
								continue;
							}

							// check for cached previous copy of array
							if( TryGetPreviouslyCachedCopyOfObject( sourceArray, out var preCachedArray ) )
							{
								fieldInfo.SetValue( target, preCachedArray );
							}
							else
							{
								var targetArray = Array.CreateInstance( arrayElementType, sourceArray.Length ) as Array;

								fieldInfo.SetValue(	target, targetArray );

								Copy( sourceArray, targetArray, copyOptions, arrayOfTypesToNotCopy, dictCachedObjectCopies, recursionCounter );
							}
						}
						continue;
					}

					if(	! TypeIsNotExcludedFromCopying( fieldTypeToCopy ) )
					{
						Debug.Log( $"{logFieldPrefix} - type is in NO COPY list ignoring" );
						continue;
					}
					
					if( IsPrimitive( fieldTypeToCopy ) )
					{
						Debug.Log( $"{logFieldPrefix} - VALUE" );
						fieldInfo.SetValue( target, fieldInfo.GetValue( source ) ); 
					}
					else
					{
						if( fieldTypeToCopy.IsSubclassOf( typeof(UnityEngine.Object) ) )
						{
							Debug.Log( $"{logFieldPrefix} - UNITY CLASS not in hierarchy ref copy" );
							fieldInfo.SetValue( target, fieldInfo.GetValue( source ) ); 
						}
						else
						{
							Debug.Log( $"{logFieldPrefix} - CLASS" );

							var sourceFieldValue = fieldInfo.GetValue( source );
							
							if( null == sourceFieldValue )
							{
								fieldInfo.SetValue( target, null );
							}
							else
							{
								// always have to replace the target reference rather than overwrite it as it could be used elsewhere
								if( TryGetPreviouslyCachedCopyOfObject( sourceFieldValue, out var alreadyCachedCopy ) )
								{
									fieldInfo.SetValue( target, alreadyCachedCopy );
								}
								else
								{
									if( TryCreateDefaultConstructedObjectOfSameTypeAndAddToCache( sourceFieldValue, out var defaultConstructed ) )
									{
										fieldInfo.SetValue( target, defaultConstructed );
										Copy( sourceFieldValue, defaultConstructed, copyOptions, arrayOfTypesToNotCopy, dictCachedObjectCopies, recursionCounter );
									}
									else
									{
										Debug.Log( $"{logFieldPrefix}: deep copy target object is null & couldn't be created - not copying" );
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

public static class Debug
{
	public static void Log( string outputText )
	{
		System.Console.WriteLine( outputText );
	}
}

namespace UnityEngine
{
	[System.Serializable]
	public class Object
	{}
}

[System.Serializable]
public class GameObject : UnityEngine.Object
{}

[System.Serializable]
public class Component : UnityEngine.Object
{}

[System.Serializable]
public class MemberClassToCopy
{
	public int			m_int;
	public int[]		m_intArray;
	public string		m_string;
	public string[]		m_stringArray;

	public MemberClassToCopy()
	{}

	public MemberClassToCopy( bool dummy )
	{
		m_int			= 123;
		m_intArray		= new []{ 1, 2, 3 };
		m_string		= "123";
		m_stringArray	= new []{ "1", "2", "3" };	
	}

	public MemberClassToCopy( int i )
	{
		m_int         = i;
		m_intArray    = new[] { i, i + 1 };
		m_string      = $"{i}";
		m_stringArray = new[] { $"{i}", $"{i + 1}" };
	}

}

[System.Serializable]
public class ComponentDerived : Component
{
	public int m_int;
}

[System.Serializable]
public class TestClassToDeepCopy
{
	public int					m_int;
	public string				m_string;
	public int[]				m_intArray;
	public string[]				m_stringArray;
	public Component			m_component;
	public ComponentDerived		m_componentDerived;
	public GameObject			m_gameObject;
	public MemberClassToCopy	m_memberClass;
	public MemberClassToCopy[]	m_memberClassArray;
	public ComponentDerived[]	m_componentDerivedArray;

	private string m_stringPrivate;
	[NonSerialized] public int m_intNon;

	public TestClassToDeepCopy()
	{}

	public TestClassToDeepCopy( bool dummy )
	{
		m_int					= 69;
		m_string				= "69";
		m_intArray				= new []{ 42, 92, 73 };
		m_stringArray			= new []{ "42", "92", "73" };
		m_memberClass			= new MemberClassToCopy( false);
		m_component				= new Component();
		m_componentDerived		= new ComponentDerived();
		m_gameObject			= new GameObject();
		m_memberClassArray		= new []{ new MemberClassToCopy( 1 ), new MemberClassToCopy( 2 ), new MemberClassToCopy( 3 ) };
		m_componentDerivedArray = new ComponentDerived[ 10 ];

		m_intNon				= 23;
		m_stringPrivate			= "private";
	}

	public TestClassToDeepCopy Clone()
	{
		return ( TestClassToDeepCopy) this.MemberwiseClone();
	}
}

[System.Serializable]
public class ClassWithContainers

{
	public List< string >		m_stringList;
	public Dictionary< string, string > m_stringToStringDict;

	public ClassWithContainers()
	{}

	public ClassWithContainers( bool dummy )
	{
		m_stringList = new List< string >();
		m_stringList.Add( "this" );
		m_stringList.Add( "is" );
		m_stringList.Add( "a" );
		m_stringList.Add( "list" );
		m_stringList.Add( "of" );
		m_stringList.Add( "strings" );

		m_stringToStringDict = new Dictionary< string, string >();
		m_stringToStringDict.Add( "this", "is" );
		m_stringToStringDict.Add( "a", "dictionary" );
		m_stringToStringDict.Add( "of", "strings" );
	}

	public ClassWithContainers( int initValue )
	{
		m_stringList = new List< string >();
		m_stringToStringDict = new Dictionary< string, string >();

		for( int i = 0; i < 3;  ++i )
		{
			var loopString = $"{initValue + i}";
			m_stringList.Add( loopString );
			m_stringToStringDict.Add( loopString, $"{loopString}_value" );
		}	
	}
}

namespace Test
{
	class Program
	{
		//
		// ERROR: this is an IMPOSSIBLE task for types using references
		// the specifics of which references are copied as values into from source to target,
		// which are cloned etc. (and how this is handled for members which are containers etc.)
		// are myriad and unguessable - any deep copy which will work must have per-object behaviour
		// requiring an interface implemented for the classe(s) in question
		//
		// this is still interesting as a failed experiment though IMO :)
		//

		static void Main( string[] args )
		{
			{
				var source = new TestClassToDeepCopy( false );
				var target = new TestClassToDeepCopy();
				DeepCopy.SourceToTarget( source, target );
			}

			{
				var source = new TestClassToDeepCopy( false );
				var target = new TestClassToDeepCopy();
				DeepCopy.SourceToTargetWithOptions( source, target, DeepCopy.CopyOptions.PublicSerialised );
			}

			{
				var source = new TestClassToDeepCopy( false );
				var target = new TestClassToDeepCopy();
				DeepCopy.SourceToTargetWithOptions( source, target, DeepCopy.CopyOptions.PublicSerialised | DeepCopy.CopyOptions.CopyNonSerialisedFields );
			}

			{
				var source = new TestClassToDeepCopy( false );
				var target = new TestClassToDeepCopy();
				DeepCopy.SourceToTargetWithOptions( source, target, DeepCopy.CopyOptions.PublicSerialised | DeepCopy.CopyOptions.CopyPrivateFields );
			}

			{
				var source = new TestClassToDeepCopy( false );
				var target = new TestClassToDeepCopy();
				DeepCopy.SourceToTargetWithFilter( source, target, new[] { typeof( GameObject ), typeof( Component ) } );
			}

			{
				var source = new ClassWithContainers( false );
				var target = new ClassWithContainers();
				DeepCopy.SourceToTarget( source, target );
			}

			{
				var source = new[] { new ClassWithContainers( 0 ), new ClassWithContainers( 1 ), new ClassWithContainers( 2 ) };
				var target = new ClassWithContainers[source.Length];
				DeepCopy.SourceToTarget( source, target );
			}

			{
				var source = new TestClassToDeepCopy( false );
				var cloned = DeepCopy.Clone( source );
			}

			{
				var source = new[] { new MemberClassToCopy( 0 ), new MemberClassToCopy( 1 ), new MemberClassToCopy( 2 ) };
				var cloned = DeepCopy.Clone( source );
			}

			Console.ReadKey();
		}
	}
}
